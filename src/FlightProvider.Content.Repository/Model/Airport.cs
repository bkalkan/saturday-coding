﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FlightProvider.Content.Repository.Model
{
    [Serializable]
        
    public class Airport
    {
        [XmlAttribute("Code")]
        public string Code { get; set; }
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("Country")]
        public string Country { get; set; }        
    }
}
