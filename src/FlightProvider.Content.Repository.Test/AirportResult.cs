﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using FlightProvider.Content.Repository.Model;

namespace FlightProvider.Content.Repository.Test
{
    [XmlRoot("root")]
    [Serializable]
    public class AirportResult
    {
        [XmlElement("item")]
        public Airport[] Airports { get; set; }
    }
}
