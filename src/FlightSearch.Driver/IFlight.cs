﻿using System.Collections.Generic;
using FlightSearch.Driver.Classes;

namespace FlightSearch.Driver
{
    public interface IFlight
    {
        List<FlightSearchResult> Search(FlightSearchCriteria criteria);
    }
}
