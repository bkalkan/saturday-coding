﻿using System;
using FlightSearch.Driver.Classes;

namespace FlightSearch.Driver
{
    class Program
    {
        private static IFlight _iflight;
        static void Main(string[] args)
        {
            _iflight = IocUtil.Resolve<IFlight>();
            _iflight.Search(new FlightSearchCriteria
            {
                FromCity = "AYT",
                ToCity = "IST",
                Adult = 2,
                Child = 0,
                DepartureDate = new DateTime(2013, 12, 29)
            });
        }
    }
}
