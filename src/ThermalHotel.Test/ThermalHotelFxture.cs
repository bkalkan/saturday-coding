﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using ThermalHotel;

namespace ThermalHOtel.Test
{
    [TestFixture]
    public class ThermalHotelFxture
    {
        [Test]
        public void Write_ThermalHotel_Description_Price()
        {
            IThermalHotel thermalHotel = new Thermal();
            thermalHotel.GetPrice().Should().Be(100);
            var family = new FamilyRoom(thermalHotel);
            family.GetPrice().Should().Be(130);
            var breakFast = new Breakfast(family);
            breakFast.GetPrice().Should().Be(135);
            //Console.WriteLine(breakFast.GetDescription()+" Total Price:"+breakFast.GetPrice());

        }
        [Test]
        public void FamilyRoom_BasePrice_Be_30()
        {
            var family=new FamilyRoom();
            family.GetPrice().Should().Be(30);
        }

    }
}
